# SPS IBS example

Example project to benchmark and develop Intra-Beam Scattering (IBS) software for Xsuite, starting with the [Xsuite test modules](https://github.com/MichZampetakis/IBS_for_Xsuite/tree/main/output) developed by Michail Zampetakis and then further developed here by Elias Waagaard and Sofia Kostoglou in the [SPS space charge example](https://gitlab.cern.ch/elwaagaa/sps_ion_space_charge_example/-/tree/master/sps_ion_space_charge_example/IBS_module).

This project aims at benchmarking and optimizing the IBS module behaviour for Xsuite tracking. 
