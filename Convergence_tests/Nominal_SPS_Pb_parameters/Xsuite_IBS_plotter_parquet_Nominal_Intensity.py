"""
Plot the intensity of the IBS module tests for given intensity and number of turns
"""
import matplotlib.pylab as plt
import pandas as pd

# Select destination folder depending on the parameter values chosen 
save_fig = True

# Set initial parameters 
n_turns = 300000 
bunch_intensity = 3.5e8   # 3.5e8 originally
sigma_z = 0.23  #0.23 original SPS parameter

# Define plot parameters 
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

fl_kinetic=True
fl_simple=False
fl_analytical=True

if fl_kinetic:
    kinetic    = pd.read_parquet("xsuite_kinetic_{}.parquet".format(n_turns))
if fl_simple:
    simple     = pd.read_parquet("xsuite_simple_{}.parquet".format(n_turns))
if fl_analytical:
    analytical = pd.read_parquet("xsuite_analytical_{}.parquet".format(n_turns))

# Also open the TBT checks for the integrals
tbt_checks = pd.read_parquet("tbt_checks_{}_turns.parquet".format(n_turns))

f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (16,5))
f.suptitle('SPS PB ion tracking: I = {:.2e}, sigma_z = {:.3f}'.format(bunch_intensity, sigma_z), fontsize=20)

# ax1.plot(nag[0], 'r')
plt.sca(ax1)
if fl_kinetic:
    plt.plot(kinetic['eps_x'].values, c='b', label='Kinetic', alpha=0.7)
if fl_simple:
    plt.plot(simple['eps_x'].values, c='g', label='Simple kick')
if fl_analytical:
    plt.plot(analytical['eps_x'].values, c='k', label='Analytical')
plt.legend(fontsize=12)

plt.sca(ax2)
if fl_kinetic:
    plt.plot(kinetic['eps_y'].values, c='b', alpha=0.7)
if fl_simple:
    plt.plot(simple['eps_y'].values, c='g')
if fl_analytical:
    plt.plot(analytical['eps_y'].values, c='k')

plt.sca(ax3)
if fl_kinetic:
    plt.plot(kinetic['sig_delta'].values*1e3, c='b', alpha=0.7)
if fl_simple:
    plt.plot(simple['sig_delta'].values*1e3, c='g')
if fl_analytical:
    plt.plot(analytical['sig_delta'].values*1e3, c='k')

ax1.set_ylabel(r'$\varepsilon_x$ [m]')
ax1.set_xlabel('Turns')

ax2.set_ylabel(r'$\varepsilon_y$ [m]')
ax2.set_xlabel('Turns')

ax3.set_ylabel(r'$\sigma_{\delta}$ [$10^{-3}$]')
ax3.set_xlabel('Turns')

plt.tight_layout()

#plt.savefig('comparison_parquet.png', dpi = 400)
plt.show()
if save_fig:
    f.savefig("IBS_tests_SPS_ions_{}_turns.png".format(n_turns), dpi=250)

# Plot the integral evolution
fig = plt.figure(figsize=(10, 7))
fig.suptitle('Nagaitsev Integrals')
ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.plot(tbt_checks['Ixx'], c='b', label='Ixx')
ax.plot(tbt_checks['Iyy'], c='g', label='Iyy')
ax.plot(tbt_checks['Ipp'], c='k', label='Ipp')
ax.set_xlabel('Turns')
ax.legend()
if save_fig:
    fig.savefig("Nagaitsev_integrals_SPS_PB_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)

# Plot the statistical emittance
fig2 = plt.figure(figsize=(10, 7))
fig2.suptitle('Statistical emittance')
ax2 = fig2.add_subplot(1, 1, 1)  # create an axes object in the figure
ax2.plot(kinetic['eps_x'].values, linestyle='dashed', c='b', alpha=0.7, label='$\\varepsilon_{x}$ kinetic')
ax2.plot(kinetic['eps_y'].values, linestyle='dashed', c='r', alpha=0.7, label='$\\varepsilon_{y}$ kinetic')
ax2.set_xlabel('Turns')
ax2.legend()
if save_fig:
    fig2.savefig("Statistical_emittances_SPS_PB_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)

# Plot the losses 
fig3 = plt.figure(figsize=(10, 7))
fig3.suptitle('Number of particles')
ax3 = fig3.add_subplot(1, 1, 1)  # create an axes object in the figure
ax3.plot(tbt_checks['particles'], c='b')
ax3.set_xlabel('Turns')
if save_fig:
    fig3.savefig("Number_of_particles_SPS_PB_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)