#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SPS protons: IBS class tester with Xsuite to compare kinetic and analytical growth rates 

This version uses line and particle input from Xsuite model of SPS sequence, including correct normalized emittance
"""
import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import pandas as pd
import matplotlib.pylab as plt
import sys
from pathlib import Path

# Import IBS class 
sys.path.append("..")
from IBS import NagaitsevIBS

# Flags to save figures
save_fig = True

## Load xsuite line
with open("../SPS_sequence/SPS_2021_LHC_q20_for_tracking.json") as fid:
    dd=json.load(fid)
line = xt.Line.from_dict(dd)
p0 = line.particle_ref

# Select destination folder depending on the parameter values chosen 
intensity_factor = 1.0
nr_kinetic_runs = 1 # add more if we want to average
plot_average = False # set to true if more than one run
destination_folder = '{:d}times_intensity_protons'.format(int(intensity_factor))
Path(destination_folder).mkdir(parents=True, exist_ok=True)


# ----- Set initial parameters -----
n_turns = 500 #00
bunch_intensity = intensity_factor*1e11/3 #1e11/3 is the original
sigma_z = 22.5e-2/3 #22.5e-2/3 is the original value, but around 0.23 is the maximum bunch length that fits in the bucket 
n_part = int(2000)
# For emittance: for SPS ions, use normalized emittance from Hannes and Isabelle's table 
nemitt_x= 2.5e-6
nemitt_y= 2.5e-6
Harmonic_Num = 4620
Energy_loss = 0
RF_Voltage = 3.0 # updated according to discussions with Alexandre Lasheen
IBS_step = 10.0 # turns to recompute IBS kick
# ----------------------------------

## Choose a context
context = xo.ContextCpu()         # For CPU

## Transfer lattice on context and compile tracking code
tracker = xt.Tracker(_context=context, line=line)
tracker.optimize_for_tracking()
particles0 = xp.generate_matched_gaussian_bunch(
         num_particles=n_part, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=p0, tracker=tracker)


tw = tracker.twiss(particle_ref = p0)
dfs_kinetic = []
dfs_analytical = []

# Initialize benchmark dictionary for the kinetic mode 
check_keys = {'epsn_x', 'epsn_y', 'particles', 'kinTx', 'kinTy', 'kinTz'}
tbt_checks = {nn: np.zeros((n_turns), dtype = float) for nn in check_keys}


#%% First investigate KINETIC mode 
mode = 'kinetic'
for j in range(nr_kinetic_runs):
    print(f"\nRUN: {j+1}\n")
    
    particles = particles0.copy()
    
    # ----- Initialize IBS -----
    IBS = NagaitsevIBS()
    IBS.set_beam_parameters(particles)
    IBS.set_optic_functions(tw)
    
    dt = 1./IBS.frev # consecutive turns/frev, used only for analytical, 
    
    ## Initialize dictionaries
    record_emit = {'eps_x', 'eps_y', 'sig_delta', 'bl', 'kinTx', 'kinTy', 'kinTz'}
    turn_by_turn = {nn: np.zeros((n_turns), dtype = float) for nn in record_emit}
    
    # --- Initialize 
    sig_x = np.std(particles.x[particles.state > 0])
    sig_y = np.std(particles.y[particles.state > 0])
    sig_delta = np.std(particles.delta[particles.state > 0])
    turn_by_turn['bl'][0]        = np.std(particles.zeta[particles.state > 0])
    turn_by_turn['sig_delta'][0] = sig_delta
    
    # Decide whether to use manual calculation of emittance or the StatisticalEmittance 
    turn_by_turn['eps_x'][0] = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
    turn_by_turn['eps_y'][0] = sig_y**2 / tw['bety'][0] 
    
    tbt_checks['epsn_x'][0] = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
    tbt_checks['epsn_y'][0] = sig_y**2 / tw['bety'][0] 
    tbt_checks['particles'][0] = sum(particles.state > 0)

    
    for i in range(1, n_turns):
    
        print(f'Run {j+1}: Turn = {i}')
        print('N_part = ',len(particles.x[particles.state > 0]))
    
        # Calculate normalized emittances 
        sig_x = np.std(particles.x[particles.state > 0])
        sig_y = np.std(particles.y[particles.state > 0])
        sig_delta = np.std(particles.delta[particles.state > 0])
        turn_by_turn['bl'][i]        = np.std(particles.zeta[particles.state > 0])
        turn_by_turn['sig_delta'][i] = sig_delta
        turn_by_turn['eps_x'][i]     = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
        turn_by_turn['eps_y'][i]     = sig_y**2 / tw['bety'][0] 
        
        tbt_checks['epsn_x'][i] = turn_by_turn['eps_x'][i]  
        tbt_checks['epsn_y'][i] = turn_by_turn['eps_y'][i]
        tbt_checks['particles'][i] = sum(particles.state > 0)
        
        # Apply the kinetic kick from the calculated coefficient 
        if (i % IBS_step == 0) or (i==1):
           IBS.calculate_kinetic_coefficients(particles)
        IBS.apply_kinetic_kick(particles)
        print(f"Dx: {IBS.Dx}, Dy: {IBS.Dy}, Dz: {IBS.Dz}")
        print(f"Fx: {IBS.Fx}, Fy: {IBS.Fy}, Fz: {IBS.Fz}")

        # Check the growth rates 
        tbt_checks['kinTx'][i] = IBS.Dx - IBS.Fx
        tbt_checks['kinTy'][i] = IBS.Dy - IBS.Fy
        tbt_checks['kinTz'][i] = IBS.Dz - IBS.Fz
    
        tracker.track(particles)

    Emitt = []
    Emitt.append(turn_by_turn['eps_x'])
    Emitt.append(turn_by_turn['eps_y'])
    Emitt.append(turn_by_turn['sig_delta'])
    Emitt.append(turn_by_turn['bl'])
    Emitt.append(tbt_checks['kinTx'])
    Emitt.append(tbt_checks['kinTy'])
    Emitt.append(tbt_checks['kinTz'])
    
    df_kinetic = pd.DataFrame(np.array(Emitt).T, columns=["eps_x", "eps_y", "sig_delta", "bl", "kinTx", "kinTy", "kinTz"])
    df_kinetic.index.name = 'Turn'
    df_kinetic.to_parquet("{}/xsuite_run{}_{}_{}.parquet".format(destination_folder, j, mode, n_turns))
    dfs_kinetic.append(df_kinetic)
  
    
#%% Then investigate ANALYTICAL kick
mode = 'analytical'
print(f"Model: {mode}")

particles = particles0.copy()

# ----- Initialize IBS -----
IBS = NagaitsevIBS()
IBS.set_beam_parameters(particles)
IBS.set_optic_functions(tw)

dt = 1./IBS.frev # consecutive turns/frev, used only for analytical, 

## Initialize dictionaries
record_emit = {'eps_x', 'eps_y', 'sig_delta', 'bl', 'Ixx', 'Iyy', 'Ipp'}
turn_by_turn_analytical = {nn: np.zeros((n_turns), dtype = float) for nn in record_emit}

# --- Initialize 
sig_x = np.std(particles.x[particles.state > 0])
sig_y = np.std(particles.y[particles.state > 0])
sig_delta = np.std(particles.delta[particles.state > 0])
turn_by_turn_analytical['bl'][0] = np.std(particles.zeta[particles.state > 0])
turn_by_turn_analytical['sig_delta'][0] = sig_delta

# Decide whether to use manual calculation of emittance or the StatisticalEmittance 
turn_by_turn_analytical['eps_x'][0] = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
turn_by_turn_analytical['eps_y'][0] = sig_y**2 / tw['bety'][0] 

for i in range(1, n_turns):

    print('Turn = ', i)
    print('N_part = ',len(particles.x[particles.state > 0]))

    # Calculate the IBS integrals and add the kick 
    if (i % IBS_step == 0) or (i==1):
         IBS.calculate_integrals(
             turn_by_turn_analytical['eps_x'][i-1],
             turn_by_turn_analytical['eps_y'][i-1],
             turn_by_turn_analytical['sig_delta'][i-1],
             turn_by_turn_analytical['bl'][i-1]
             )
    Emit_x, Emit_y, Sig_M = IBS.emit_evol(turn_by_turn_analytical['eps_x'][i-1],
                                          turn_by_turn_analytical['eps_y'][i-1],
                                          turn_by_turn_analytical['sig_delta'][i-1],
                                          turn_by_turn_analytical['bl'][i-1], 
                                          dt
                                          )
    
    Sigma_E = Sig_M*IBS.betar**2
    BunchL = IBS.BunchLength(IBS.Circu, Harmonic_Num, IBS.EnTot, IBS.slip, 
                   Sigma_E, IBS.betar, RF_Voltage*1e-3, Energy_loss, IBS.Ncharg)
    
    turn_by_turn_analytical['bl'][i]        = BunchL
    turn_by_turn_analytical['sig_delta'][i] = Sig_M
    turn_by_turn_analytical['eps_x'][i]     = Emit_x
    turn_by_turn_analytical['eps_y'][i]     = Emit_y
  
    turn_by_turn_analytical['Ixx'][i] = IBS.Ixx
    turn_by_turn_analytical['Iyy'][i] = IBS.Iyy
    turn_by_turn_analytical['Ipp'][i] = IBS.Ipp  

    tracker.track(particles)

Emitt = []
Emitt.append(turn_by_turn_analytical['eps_x'])
Emitt.append(turn_by_turn_analytical['eps_y'])
Emitt.append(turn_by_turn_analytical['sig_delta'])
Emitt.append(turn_by_turn_analytical['bl'])
Emitt.append(turn_by_turn_analytical['Ixx'])
Emitt.append(turn_by_turn_analytical['Iyy'])
Emitt.append(turn_by_turn_analytical['Ipp'])

df_analytical = pd.DataFrame(np.array(Emitt).T, columns=["eps_x", "eps_y", "sig_delta", "bl", 'Ixx', 'Iyy', 'Ipp'])
df_analytical.index.name = 'Turn'
df_analytical.to_parquet("{}/xsuite_{}_{}.parquet".format(destination_folder, mode, n_turns))
        
# Also save the TBT check
df_tbt = pd.DataFrame(tbt_checks)
df_tbt.to_parquet("{}/tbt_checks_{}_turns.parquet".format(destination_folder, n_turns))
  
# ------------------------ DEFINE PLOT PARAMETERS --------------------------------------
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# ----------------------- PLOT THE FIGURE ---------------------------------------------
fl_kinetic=True
fl_analytical=True

if fl_analytical:
    analytical = df_analytical

f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (16,5))
f.suptitle('SPS proton tracking: I = {:.2e}, sigma_z = {:.3f}'.format(bunch_intensity, sigma_z), fontsize=20)

# Find the average kinetic kick
if nr_kinetic_runs == 1:
    eps_x_mean = np.array(dfs_kinetic[0]['eps_x'])
    eps_y_mean = np.array(dfs_kinetic[0]['eps_y'])
    sig_delta_mean = np.array(dfs_kinetic[0]['sig_delta'])
elif nr_kinetic_runs ==3: 
    All_epsx = np.array([dfs_kinetic[0]['eps_x'], dfs_kinetic[1]['eps_x'], dfs_kinetic[2]['eps_x']])
    All_epsy = np.array([dfs_kinetic[0]['eps_y'], dfs_kinetic[1]['eps_y'], dfs_kinetic[2]['eps_y']])
    All_sig_delta = np.array([dfs_kinetic[0]['sig_delta'], dfs_kinetic[1]['sig_delta'], dfs_kinetic[2]['sig_delta']])
    All_BL = np.array([dfs_kinetic[0]['bl'], dfs_kinetic[1]['bl'], dfs_kinetic[2]['bl']])
    eps_x_mean = np.mean(All_epsx, axis=0)
    eps_y_mean = np.mean(All_epsy, axis=0)
    sig_delta_mean = np.mean(All_sig_delta, axis=0)
    bl_mean = np.mean(All_BL, axis=0)
else:
    print("Set nr of kinetic runs to 3")

# ax1.plot(nag[0], 'r')
plt.sca(ax1)
if fl_kinetic:
    if plot_average:
        plt.plot(eps_x_mean, alpha=0.7, label='Mean Kinetic {} runs'.format(nr_kinetic_runs), c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['eps_x'].values, alpha=0.7, label=f'Kinetic {k+1}')
if fl_analytical:
    plt.plot(analytical['eps_x'].values, c='k', label='Analytical')
plt.legend(fontsize=12)

plt.sca(ax2)
if fl_kinetic:
    if plot_average:
        plt.plot(eps_y_mean, alpha=0.7, c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['eps_y'].values, alpha=0.7)
if fl_analytical:
    plt.plot(analytical['eps_y'].values, c='k')

plt.sca(ax3)
if fl_kinetic:
    if plot_average:
        plt.plot(sig_delta_mean*1e3, alpha=0.7, c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['sig_delta'].values*1e3, alpha=0.7)
if fl_analytical:
    plt.plot(analytical['sig_delta'].values*1e3, c='k')

ax1.set_ylabel(r'$\varepsilon_x$ [m]')
ax1.set_xlabel('Turns')

ax2.set_ylabel(r'$\varepsilon_y$ [m]')
ax2.set_xlabel('Turns')

ax3.set_ylabel(r'$\sigma_{\delta}$ [$10^{-3}$]')
ax3.set_xlabel('Turns')

plt.tight_layout()
plt.show()
if save_fig:
    f.savefig("{}/Emittances_SPS_PB_IBS_tracking_{}_turns.png".format(destination_folder, n_turns), dpi=250)
    
# Plot the integral evolution
fig, (ax11, ax22, ax33) = plt.subplots(1, 3, figsize = (16,5))
fig.suptitle('SPS proton tracking. IBS: Growth Rates')

ax11.plot(df_kinetic["kinTx"][1:], marker='o', linestyle=None, markerfacecolor='none', label='Kinetic: Tx (last run)')
ax11.plot(df_analytical['Ixx'][1:], c='b', linewidth=4, label='Analytical: Ixx')
ax11.set_xlabel('Turns')
#ax11.set_yscale('log')
ax11.legend(fontsize=12)

ax22.plot(df_kinetic["kinTy"][1:], marker='o', markersize=8, linestyle=None, markerfacecolor='none', label='Kinetic: Ty (last run)')
ax22.plot(df_analytical['Iyy'][1:], c='g', linewidth=4, label='Analytical: Iyy')
ax22.set_xlabel('Turns')
#ax22.set_yscale('log')
ax22.legend(fontsize=12)

ax33.plot(df_kinetic["kinTz"][1:], marker='o', markersize=8,  linestyle=None, markerfacecolor='none', label='Kinetic: Tz (last run)')
ax33.plot(df_analytical['Ipp'][1:], c='k', linewidth=4, label='Ipp')
ax33.set_xlabel('Turns')
#ax33.set_yscale('log')
ax33.legend(fontsize=12)

if save_fig:
    fig.savefig("{}/IBS_Growth_SPS_Pb_IBS_tracking_{}_turns.png".format(destination_folder, n_turns), dpi=250)

# Plot the bunch length evolution 
fig2, ax = plt.subplots(1, 1, figsize = (10,5))
fig2.suptitle('SPS proton tracking. IBS: Bunch lengths')
ax.plot(df_analytical['bl'][1:], c='r', label='Analytical')
if plot_average:
    ax.plot(bl_mean[1:], c='b', label='Mean Kinetic')
else:
    ax.plot(df_kinetic['bl'][1:], c='b', label='Kinetic (from particles object)')
ax.set_xlabel('Turns')
ax.set_ylabel('Bunch Length')
#ax.set_yscale('log')
ax.legend()
if save_fig:
    fig2.savefig("{}/BunchLength_SPS_Pb_IBS_tracking_{}_turns.png".format(destination_folder, n_turns), dpi=250)
    
fig3, ax3 = plt.subplots(1, 1, figsize = (10,5))
fig3.suptitle('SPS Proton tracking. IBS: Longitudinal Phase Space')
ax3.plot(particles.zeta, particles.delta*1000, '.', markersize=3)
ax3.set_xlabel(r'z [-]')
ax3.set_ylabel(r'$\delta$ [1e-3]')