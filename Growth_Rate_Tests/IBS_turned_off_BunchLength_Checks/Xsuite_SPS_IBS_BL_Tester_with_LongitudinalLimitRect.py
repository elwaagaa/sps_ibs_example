#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SPS Pb ions: IBS class tester with Xsuite to compare kinetic and analytical growth rates 
- check first and second moments of bunch length
- add Longitudinal Limit Rect to remove particles that are outside the bucket 

This version uses line and particle input from Xsuite model of SPS sequence, including correct normalized emittance
"""
import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import matplotlib.pylab as plt
import sys
import pandas as pd

# Import IBS class 
sys.path.append("../..")
from IBS import NagaitsevIBS

## Load xsuite line
with open("../../SPS_sequence/SPS_2021_Pb_ions_for_tracking.json") as fid:
    dd=json.load(fid)
line = xt.Line.from_dict(dd)
p0 = line.particle_ref

# Select destination folder depending on the parameter values chosen 
intensity_factor = 1

# ----- Elias: Nominal SPS initial parameters -----
n_turns = 300 #00
bunch_intensity = intensity_factor*3.5e8   # 3.5e8 originally
sigma_z = 0.23  #0.23 original SPS parameter
n_part = int(5000)
# For emittance: for SPS ions, use normalized emittance from Hannes and Isabelle's table 
nemitt_x= 1.3e-6
nemitt_y= 0.9e-6  
Harmonic_Num = 4653
Energy_loss = 0
RF_Voltage = 3.0 # 3.0 updated according to discussions with Alexandre Lasheen
IBS_step = 20.0 # turns to recompute IBS kick
# ----------------------------------

# Add longitudinal limit rectangle 
bucket_length = line.get_length()/Harmonic_Num
line.unfreeze() # if you had already build the tracker
line.append_element(element=xt.LongitudinalLimitRect(min_zeta=-bucket_length/2, max_zeta=bucket_length/2), name='long_limit')
line.build_tracker()

## Choose a context
context = xo.ContextCpu()         # For CPU

## Transfer lattice on context and compile tracking code
tracker = xt.Tracker(_context=context, line=line)
tracker.optimize_for_tracking()
particles0 = xp.generate_matched_gaussian_bunch(
         num_particles=n_part, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=p0, tracker=tracker)


tw = tracker.twiss(particle_ref = p0)

particles = particles0.copy()

# ----- Initialize IBS -----
IBS = NagaitsevIBS()
IBS.set_beam_parameters(particles)
IBS.set_optic_functions(tw)

dt = 1./IBS.frev # consecutive turns/frev, used only for anadf_tbtchecks = pd.DataFramelytical, 

## Initialize dictionaries for tbt checks 
columns = {'sig_x', 'sig_y', 'sig_delta', 'mean_delta', 'sig_z', 'BL_theoretical', 'NrParticles'}
tbt_checks = {nn: np.zeros((n_turns), dtype = float) for nn in columns}


# Track and investigate stability of bunch length
for i in range(n_turns):
    print('Turn = ', i)
    print('N_part = ',len(particles.x[particles.state > 0]))

    # Calculate RF bucket length here, and "kill" particles that are outside 

    tbt_checks['sig_x'][i] = np.std(particles.x[particles.state > 0])
    tbt_checks['sig_y'][i] = np.std(particles.y[particles.state > 0])
    tbt_checks['sig_delta'][i] = np.std(particles.delta[particles.state > 0])
    tbt_checks['mean_delta'][i] = np.mean(particles.delta[particles.state > 0])
    tbt_checks['sig_z'][i]  = np.std(particles.zeta[particles.state > 0])
    tbt_checks['NrParticles'][i]  = len(particles.zeta[particles.state > 0])
    
    # Check theoretical bunch length 
    Sigma_E = tbt_checks['sig_delta'][i]*IBS.betar**2
    BunchL = IBS.ion_BunchLength(IBS.Circu, Harmonic_Num, IBS.EnTot, IBS.slip, 
               Sigma_E, IBS.betar, RF_Voltage*1e-3, Energy_loss, IBS.Ncharg)
    tbt_checks['BL_theoretical'][i] = BunchL

    tracker.track(particles)

df_tbtchecks = pd.DataFrame(tbt_checks)
df_tbtchecks.to_parquet("Plots_LimitRect/tbt_checks_{}turns.parquet".format(n_turns))

# ------------------------ DEFINE PLOT PARAMETERS --------------------------------------
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# Plot the figures
fig, (ax1, ax2) = plt.subplots(2, 1, figsize = (12,7))
fig.suptitle('SPS Pb ions - nominal parameters: Delta mean and spread', fontsize=18)
ax1.plot(tbt_checks['mean_delta'], 'b', label='Mean($\delta$)')
ax1.legend() 
ax2.plot(tbt_checks['sig_delta'], 'r', label='$\\sigma_{\delta}$')
ax2.legend() 
fig.savefig(f'Plots_LimitRect/SPS_Pb_ions_Nominal_parameter_check_DELTA_{n_turns}turns.png', dpi=250)

fig2, (ax3, ax4) = plt.subplots(2, 1, figsize = (10,7))
fig2.suptitle('SPS Pb ions - nominal parameters: Bunch Length', fontsize=18)
ax3.plot(tbt_checks['sig_z'], 'g', label='$\\sigma_{z}$ from particles')
ax3.legend() 
ax4.plot(tbt_checks['BL_theoretical'], 'k', label='IBS theoretical Bunch Length')
ax4.legend() 
fig2.savefig(f'Plots_LimitRect/SPS_Pb_ions_Nominal_parameter_check_BL_{n_turns}turns.png', dpi=250)

fig3, ax5 = plt.subplots(1, 1, figsize = (10,5))
fig3.suptitle('SPS PB ion tracking. Last turn Longitudinal Phase Space')
ax5.plot(particles.zeta, particles.delta*1000, '.', markersize=3)
ax5.axvline(x=bucket_length/2, color='r', linestyle='dashed')
ax5.axvline(x=-bucket_length/2, color='r', linestyle='dashed')
ax5.set_xlabel(r'z [-]')
ax5.set_ylabel(r'$\delta$ [1e-3]')
fig3.savefig(f'Plots_LimitRect/SPS_Pb_ions_Nominal_parameter_check_BUCKET_{n_turns}turns.png', dpi=250)

fig4, ax6 = plt.subplots(1, 1, figsize = (10,5))
fig4.suptitle('SPS PB ion tracking. Particles survival')
ax6.plot(tbt_checks['NrParticles'], 'g')
fig4.savefig(f'Plots_LimitRect/SPS_Pb_ions_Nominal_parameter_check_SURVIVAL_{n_turns}turns.png', dpi=250)