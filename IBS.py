"""
IBS module, modified from M. Zampetakis in https://github.com/MichZampetakis/IBS_for_Xsuite
- this version also contains general functions to avoid a star import 
"""

import numpy as np
import scipy
import scipy.integrate as integrate
from scipy.interpolate import interp1d
from scipy.constants import c, hbar, m_e
from scipy.constants import physical_constants
from scipy.integrate import fixed_quad

class NagaitsevIBS():
    """
    Class object to calculate the Nagaitsev Integrals and the momentum kicks for the emittance evolution
    """
    def __init__(self, *args, **kwargs):
        pass

    def _Phi(self, beta , alpha , eta , eta_d):
        """
        Lattice function, Eq 2.37e from Bjorken-Mtingwa (1983) 
        """
        return eta_d + alpha * eta / beta

    def set_beam_parameters(self, particles):
        """
        Set beam parameters given a particle object
        """
        self.Npart  = particles.weight[0] * particles.gamma0.shape[0]
        self.Ncharg = particles.q0
        self.EnTot  = particles.p0c[0] * 1e-9
        self.E_rest = particles.mass0 * 1e-9
        self.gammar = particles.gamma0[0]
        self.betar  = particles.beta0[0]
        E0p = physical_constants["proton mass energy equivalent in MeV"][0]*1e-3
        particle_mass_GEV = particles.mass0 * 1e-9 
        mi  = (particle_mass_GEV * scipy.constants.m_p) / E0p
        self.c_rad = (particles.q0 * scipy.constants.e)**2 / (4 * np.pi * scipy.constants.epsilon_0 * scipy.constants.c**2 * mi)

    def set_optic_functions(self, twiss):
        """
        Set optics function for IBS integral calculation for given Twiss 
        """
        self.posit  = twiss['s']
        self.Circu  = twiss['s'][-1]
        self.bet_x  = twiss['betx']
        self.bet_y  = twiss['bety']
        self.alf_x  = twiss['alfx']
        self.alf_y  = twiss['alfy']
        self.eta_x  = twiss['dx']
        self.eta_dx = twiss['dpx']
        self.eta_y  = twiss['dy']
        self.eta_dy = twiss['dpy']
        self.slip   = twiss['slip_factor']
        self.phi_x  = self._Phi(twiss['betx'], twiss['alfx'], twiss['dx'], twiss['dpx'])
        self.frev   = self.betar * c / self.Circu
        bx_b = interp1d(twiss['s'], twiss['betx'])
        by_b = interp1d(twiss['s'], twiss['bety'])
        dx_b = interp1d(twiss['s'], twiss['dx'])
        dy_b = interp1d(twiss['s'], twiss['dy'])
        self.bx_bar = integrate.quad(bx_b, twiss['s'][0], twiss['s'][-1])[0] / self.Circu
        self.by_bar = integrate.quad(by_b, twiss['s'][0], twiss['s'][-1])[0] / self.Circu
        self.dx_bar = integrate.quad(dx_b, twiss['s'][0], twiss['s'][-1])[0] / self.Circu
        self.dy_bar = integrate.quad(dy_b, twiss['s'][0], twiss['s'][-1])[0] / self.Circu

    def CoulogConst(self, Emit_x, Emit_y, Sig_M, BunchL):
        """
        Coulog constants from emittances and bunch lengths 
        """
        Etrans = 5e8 * (self.gammar * self.EnTot - self.E_rest) * (Emit_x / self.bx_bar)
        TempeV = 2.0 * Etrans
        sigxcm = 100 * np.sqrt(Emit_x * self.bx_bar + (self.dx_bar * Sig_M)**2)
        sigycm = 100 * np.sqrt(Emit_y * self.by_bar + (self.dy_bar * Sig_M)**2)
        sigtcm = 100 * BunchL
        volume = 8.0 * np.sqrt(np.pi**3) * sigxcm * sigycm * sigtcm
        densty = self.Npart  / volume
        debyul = 743.4 * np.sqrt(TempeV / densty) / self.Ncharg
        rmincl = 1.44e-7 * self.Ncharg**2 / TempeV
        rminqm = hbar * c * 1e5 / (2.0 * np.sqrt(2e-3 * Etrans * self.E_rest))
        rmin   = max(rmincl,rminqm)
        rmax   = min(sigxcm,debyul)
        coulog = np.log(rmax/rmin)
        Ncon   = self.Npart * self.c_rad**2 * c / (12 * np.pi * self.betar**3 * self.gammar**5 * BunchL)
        return Ncon * coulog

    def line_density(self, n_slices, particles):
        """
        Calculate line density of a particle object
        """
        zeta = particles.zeta[particles.state > 0]
        z_cut_head = np.max(zeta)
        z_cut_tail = np.min(zeta)
        slice_width = (z_cut_head - z_cut_tail) / float(n_slices)

        bin_edges = np.linspace(z_cut_tail - 1e-7 * slice_width,  z_cut_head + 1e-7 * slice_width,
                                num=n_slices+1, dtype=np.float64)
        bin_centers = (bin_edges[:-1] + bin_edges[1:]) / 2.

        bunch_length_rms = np.std(zeta)
        factor_distribution = bunch_length_rms * 2 * np.sqrt(np.pi)

        counts_normed, bin_edges = np.histogram(zeta, bin_edges, density=True)
        Rho_normed = np.interp(zeta, bin_centers, counts_normed * factor_distribution)
        kick_factor_normed = np.mean(Rho_normed)
        return Rho_normed
    
    def RDiter(self, x, y, z):
        """
        Calculate elliptic integrals for the Nagaitsev integrals 
        """
        R = []
        for i, j, k in zip(x, y, z):
            x0 = i
            y0 = j
            z0 = k
            if (x0 < 0) and (y0 <= 0) and (z0 <= 0):
                print('Elliptic Integral Calculation Failed. Wrong input values!')
                return
            x = x0
            y = y0
            z = [z0]
            li = []
            Sn = []
            differ = 10e-4  # originally 10e-4
            for n in range(0,1000):  #originally 1000
                xi = x
                yi = y
                li.append( np.sqrt(xi * yi) + np.sqrt(xi * z[n]) + np.sqrt(yi * z[n]) )
                x = (xi + li[n]) / 4.
                y = (yi + li[n]) / 4.
                z.append((z[n] + li[n]) / 4.)
                if ((abs(x - xi)/x0 < differ) and (abs(y - yi)/y0 < differ) and (abs(z[n] - z[n+1])/z0 < differ)): 
                    break
            lim = n
            mi = (xi + yi + 3 * z[lim]) / 5.
            Cx = 1 - (xi / mi)
            Cy = 1 - (yi / mi)
            Cz = 1 - (z[n] / mi)
            En = max(Cx,Cy,Cz)
            if En >= 1 :
                print('Something went wrong with En')
                return
            summ = 0
            for m in range(2,6): Sn.append( (Cx**m + Cy**m + 3 * Cz**m) / (2 * m) )
            for m in range(0,lim): summ += 1 / (np.sqrt(z[m]) * (z[m] + li[m]) * 4**m)

            Ern = 3 * En**6 / (1 - En)**(3/2.)
            rn = - Sn[2-2]**3 / 10. + 3 * Sn[3-2]**2 / 10. + 3 * Sn[2-2] * Sn[4-2] / 5.
            R.append(3 * summ + (1 + 3 * Sn[2-2] / 7. + Sn[3-2] / 3. + 3 * Sn[2-2]**2 / 22. + 3 * Sn[4-2] / 11. + 3 * Sn[2-2] * Sn[3-2] / 13. + 3 * Sn[5-2] / 13. + rn) / (4**lim * mi**(3/2.)))
        return R

    def Nagaitsev_Integrals(self, Emit_x, Emit_y, Sig_M, BunchL):
        """
        IBS growth rates from Nagaitsev's Integrals
        """
        const = self.CoulogConst(Emit_x, Emit_y, Sig_M, BunchL)
        sigx  = np.sqrt(self.bet_x * Emit_x + (self.eta_x * Sig_M)**2)
        sigy  = np.sqrt(self.bet_y * Emit_y + (self.eta_y * Sig_M)**2)
        ax = self.bet_x / Emit_x
        ay = self.bet_y / Emit_y
        a_s = ax * (self.eta_x**2 / self.bet_x**2 + self.phi_x**2) + 1 / Sig_M**2
        a1  = (ax + self.gammar**2 * a_s) / 2.
        a2  = (ax - self.gammar**2 * a_s) / 2.
        denom = np.sqrt(a2**2 + self.gammar**2 * ax**2 * self.phi_x**2)
        #--------------------------------------------------------------------------------
        l1 = ay
        l2 = a1 + denom
        l3 = a1 - denom
        #--------------------------------------------------------------------------------
        R1 = self.RDiter(1/l2, 1/l3, 1/l1) / l1
        R2 = self.RDiter(1/l3, 1/l1, 1/l2) / l2
        R3 = 3 * np.sqrt(l1 * l2 / l3) - l1 * R1 / l3 - l2 * R2 / l3
        #--------------------------------------------------------------------------------
        Nagai_Sp  = ( 2 * R1 - R2 * (1 - 3 * a2 / denom) - R3 * (1 + 3 * a2 / denom) ) * 0.5 * self.gammar**2
        Nagai_Sx  = ( 2 * R1 - R2 * (1 + 3 * a2 / denom) - R3 * (1 - 3 * a2 / denom) ) * 0.5
        Nagai_Sxp = 3 * self.gammar**2 * self.phi_x**2 * ax * (R3 - R2) / denom
        #--------------------------------------------------------------------------------
        Ixi = self.bet_x / (self.Circu * sigx * sigy) * (Nagai_Sx + Nagai_Sp * (self.eta_x**2 / self.bet_x**2 + self.phi_x**2) + Nagai_Sxp)
        Iyi = self.bet_y / (self.Circu * sigx * sigy) * (R2 + R3 - 2 * R1)
        Ipi = Nagai_Sp   / (self.Circu * sigx * sigy)
        #--------------------------------------------------------------------------------
        Ix = np.sum(Ixi[:-1] * np.diff(self.posit)) * const / Emit_x
        Iy = np.sum(Iyi[:-1] * np.diff(self.posit)) * const / Emit_y
        Ip = np.sum(Ipi[:-1] * np.diff(self.posit)) * const / Sig_M**2
        return Ix, Iy, Ip

    def calculate_integrals(self, Emit_x, Emit_y, Sig_M, BunchL):
        """
        Calculate and store growth rate for emittance evolution 
        """
        self.Ixx, self.Iyy, self.Ipp = self.Nagaitsev_Integrals(Emit_x, Emit_y, Sig_M, BunchL)

    def emit_evol(self, Emit_x, Emit_y, Sig_M, BunchL, dt):
        """
        Evaluates the emittance evolution using Nagaitsev's Integrals
        """
        Evolemx = Emit_x * np.exp(dt * float(self.Ixx))
        Evolemy = Emit_y * np.exp(dt * float(self.Iyy))
        EvolsiM = Sig_M  * np.exp(dt * float(0.5 * self.Ipp))

        return Evolemx, Evolemy, EvolsiM

    def emit_evol_with_SR(self, Emit_x, Emit_y, Sig_M, BunchL, EQemitX, EQemitY, EQsigmM, tau_x, tau_y, tau_s, dt):
        """
        Evaluates the emittance evolution using Nagaitsev's Integrals, including Synchrotron Radiation.
        Result in Damping Time [s], not in turns
        """
        Evolemx = (- EQemitX + np.exp(dt * 2 * (float(self.Ixx / 2.) - 1.0 / tau_x)) 
                   * (EQemitX + Emit_x * (float(self.Ixx / 2.) * tau_x - 1.0))) / (float(self.Ixx / 2.) * tau_x - 1.0)
        Evolemy = (- EQemitY + np.exp(dt * 2 * (float(self.Iyy / 2.) - 1.0 / tau_y)) 
                   * (EQemitY + Emit_y * (float(self.Iyy / 2.) * tau_y - 1.0))) / (float(self.Iyy / 2.) * tau_y - 1.0)
        EvolsiM = np.sqrt((- EQsigmM**2 + np.exp(dt * 2 * (float(self.Ipp / 2.) - 1.0 / tau_s)) 
                           * (EQsigmM**2 + Sig_M **2 * (float(self.Ipp / 2.) * tau_s - 1.0))) / (float(self.Ipp / 2.) * tau_s - 1.0))
        
        return Evolemx, Evolemy, EvolsiM

    # ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ !
    # ! ~~~~~~~~~~~~~~~~ Simple Kick ~~~~~~~~~~~~~~~~~~ !
    # ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ !
    def emit_evol_simple_kicks(self, particles):
        """
        From given particle distribution get simple kick 
        """
        Sig_x = np.std(particles.x[particles.state > 0])
        Sig_y = np.std(particles.y[particles.state > 0])
        Sig_zeta  = np.std(particles.zeta[particles.state > 0])
        Sig_delta = np.std(particles.delta[particles.state > 0])
        
        Emit_x = (Sig_x**2 - (self.eta_x[0] * Sig_delta)**2) / self.bet_x[0]
        Emit_y = Sig_y**2 / self.bet_y[0]

        Sig_px_norm = np.std(particles.px[particles.state > 0]) / np.sqrt(1 + self.alf_x[0]**2)
        Sig_py_norm = np.std(particles.py[particles.state > 0]) / np.sqrt(1 + self.alf_y[0]**2)

        Ixx, Iyy, Ipp = self.Nagaitsev_Integrals(Emit_x, Emit_y, Sig_delta, Sig_zeta)

        if Ixx < 0 : Ixx = 0
        if Iyy < 0 : Iyy = 0
        if Ipp < 0 : Ipp = 0

        DSx = Sig_px_norm * np.sqrt(2 * Ixx / self.frev)
        DSy = Sig_py_norm * np.sqrt(2 * Iyy / self.frev)
        DSz = Sig_delta   * np.sqrt(2 * Ipp / self.frev) * self.betar**2

        return DSx, DSy, DSz

    def calculate_simple_kick(self, particles):
        """
        Calculate simply kick strength 
        """
        self.DSx, self.DSy, self.DSz = self.emit_evol_simple_kicks(particles)
   
    def apply_simple_kick(self, particles):
        """
        Applies simply kick, should be used on turn-by-turn basis 
        """
        rho = self.line_density(40, particles)
        Dkick_x = np.random.normal(loc = 0, scale = self.DSx,
                                   size = particles.px[particles.state > 0].shape[0]) * np.sqrt(rho)
        Dkick_y = np.random.normal(loc = 0, scale = self.DSy,
                                   size = particles.py[particles.state > 0].shape[0]) * np.sqrt(rho)
        Dkick_p = np.random.normal(loc = 0, scale = self.DSz,
                                   size = particles.delta[particles.state > 0].shape[0]) * np.sqrt(rho)
        
        particles.px[particles.state > 0] += Dkick_x
        particles.py[particles.state > 0] += Dkick_y
        particles.delta[particles.state > 0] += Dkick_p


    # ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ !
    # ! ~~~~~~~~~~~~~~~~ Kinetic Kick ~~~~~~~~~~~~~~~~~~ !
    # ! ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ !
    def Kinetic_Coefficients(self, Emit_x, Emit_y, Sig_M, BunchL):
        """
        Calculates the kinetic kick coefficients from emittances and bunch length 
        """
        const = self.CoulogConst(Emit_x, Emit_y, Sig_M, BunchL)
        sigx  = np.sqrt(self.bet_x * Emit_x + (self.eta_x * Sig_M)**2)
        sigy  = np.sqrt(self.bet_y * Emit_y + (self.eta_y * Sig_M)**2)
        ax = self.bet_x / Emit_x
        ay = self.bet_y / Emit_y
        a_s = ax * (self.eta_x**2 / self.bet_x**2 + self.phi_x**2) + 1 / Sig_M**2
        a1  = (ax + self.gammar**2 * a_s) / 2.
        a2  = (ax - self.gammar**2 * a_s) / 2.
        denom = np.sqrt(a2**2 + self.gammar**2 * ax**2 * self.phi_x**2)
        #--------------------------------------------------------------------------------
        l1 = ay
        l2 = a1 + denom
        l3 = a1 - denom
        #--------------------------------------------------------------------------------
        R1 = self.RDiter(1/l2, 1/l3, 1/l1) / l1
        R2 = self.RDiter(1/l3, 1/l1, 1/l2) / l2
        R3 = 3 * np.sqrt(l1 * l2 / l3) - l1 * R1 / l3 - l2 * R2 / l3
        #--------------------------------------------------------------------------------
        D_Sp = 0.5 * self.gammar**2 * (2 * R1 + R2 * (1 + a2 / denom) + R3 * (1 - a2 / denom))
        F_Sp = 1.0 * self.gammar**2 * (R2 * (1 - a2 / denom) + R3 * (1 + a2 / denom))
        D_Sx = 0.5 * (2 * R1 + R2 * (1 - a2 / denom) + R3 * (1 + a2 / denom))
        F_Sx = 1.0 * (R2 * (1 + a2 / denom) + R3 * (1 - a2 / denom))
        D_Sxp = 3. * self.gammar**2 * self.phi_x**2 * ax * (R3 - R2) / denom
        
        Dxi = self.bet_x / (self.Circu * sigx * sigy) * (D_Sx + D_Sp * (self.eta_x**2 / self.bet_x**2 + self.phi_x**2) + D_Sxp)
        Fxi = self.bet_x / (self.Circu * sigx * sigy) * (F_Sx + F_Sp * (self.eta_x**2 / self.bet_x**2 + self.phi_x**2))
        Dyi = self.bet_y / (self.Circu * sigx * sigy) * (R2 + R3)
        Fyi = self.bet_y / (self.Circu * sigx * sigy) * (2 * R1)
        Dzi = D_Sp / (self.Circu * sigx * sigy)
        Fzi = F_Sp / (self.Circu * sigx * sigy)
        
        Dx = np.sum(Dxi[:-1] * np.diff(self.posit)) * const / Emit_x
        Dy = np.sum(Dyi[:-1] * np.diff(self.posit)) * const / Emit_y
        Dz = np.sum(Dzi[:-1] * np.diff(self.posit)) * const / Sig_M**2
        Fx = np.sum(Fxi[:-1] * np.diff(self.posit)) * const / Emit_x
        Fy = np.sum(Fyi[:-1] * np.diff(self.posit)) * const / Emit_y
        Fz = np.sum(Fzi[:-1] * np.diff(self.posit)) * const / Sig_M**2

        self.kinTx, self.kinTy, self.kinTz = Dx - Fx, Dy - Fy, Dz - Fz

        return Dx, Fx, Dy, Fy, Dz, Fz       # units [1/s]


    def calculate_kinetic_coefficients(self, particles):
        """
        Calculates and saves the kinetic coefficients; to be used for the kinetic kick
        """
        Sig_x = np.std(particles.x[particles.state > 0])
        Sig_y = np.std(particles.y[particles.state > 0])
        Sig_zeta  = np.std(particles.zeta[particles.state > 0])
        Sig_delta = np.std(particles.delta[particles.state > 0])
        
        Emit_x = (Sig_x**2 - (self.eta_x[0] * Sig_delta)**2) / self.bet_x[0]
        Emit_y = Sig_y**2 / self.bet_y[0]

        self.Dx, self.Fx, self.Dy, self.Fy, self.Dz, self.Fz = self.Kinetic_Coefficients(Emit_x, Emit_y, Sig_delta, Sig_zeta)

    def apply_kinetic_kick(self, particles):   
        """
        Apply the kinetic kick 
        """
        dt = 1 / self.frev      #needs to be changed.
        Ran1 = np.random.normal(loc = 0, scale = 1, size = particles.px[particles.state > 0].shape[0])
        Ran2 = np.random.normal(loc = 0, scale = 1, size = particles.py[particles.state > 0].shape[0])
        Ran3 = np.random.normal(loc = 0, scale = 1, size = particles.delta[particles.state > 0].shape[0])

        Sig_px_norm = np.std(particles.px[particles.state > 0]) / np.sqrt(1 + self.alf_x[0]**2)
        Sig_py_norm = np.std(particles.py[particles.state > 0]) / np.sqrt(1 + self.alf_y[0]**2)
        Sig_delta = np.std(particles.delta[particles.state > 0])

        rho = self.line_density(40, particles) # number of slices

        # !---------- Friction ----------!
        print(np.mean(particles.px[particles.state > 0]), np.mean(particles.py[particles.state > 0]), np.mean(particles.delta[particles.state > 0]))

        particles.px[particles.state > 0] -= self.Fx * (particles.px[particles.state > 0]-np.mean(particles.px[particles.state > 0]) ) * dt * rho     # kick units [1]
        particles.py[particles.state > 0] -= self.Fy * (particles.py[particles.state > 0]-np.mean(particles.py[particles.state > 0]) ) * dt * rho     # kick units [1]
        particles.delta[particles.state > 0] -= self.Fz * (particles.delta[particles.state > 0]-np.mean(particles.delta[particles.state > 0])) * dt * rho     # kick units [1]
                         
        # !---------- Diffusion ----------!
        particles.px[particles.state > 0] += Sig_px_norm * np.sqrt(2 * dt * self.Dx) * Ran1 * np.sqrt(rho)     # kick units [1]
        particles.py[particles.state > 0] += Sig_py_norm * np.sqrt(2 * dt * self.Dy) * Ran2 * np.sqrt(rho)     # kick units [1]
        particles.delta[particles.state > 0] += Sig_delta* np.sqrt(2 * dt * self.Dz) * Ran3 * np.sqrt(rho)     # kick units [1]

    def BunchLength(self, Circumferance, Harmonic_Num, Energy_total, SlipF, Sigma_E, beta_rel, RF_Voltage, Energy_loss, Z):
        """
        Returns bunch length of protons for given parameters, from Wiedermann's accelerator book
        """
        return Sigma_E * Circumferance * np.sqrt(abs(SlipF) * Energy_total / (2 * np.pi * beta_rel * Harmonic_Num * np.sqrt(Z**2 * RF_Voltage**2 - Energy_loss**2)))

    def EnergySpread(self, Circumferance, Harmonic_Num, Energy_total, SlipF, BL, beta_rel, RF_Voltage, Energy_loss, Z):
        """
        Returns energy spread of protons for given parameters, from Wiedermann's accelerator book
        """
        return BL / (Circumferance * np.sqrt(abs(SlipF) * Energy_total / (2 * np.pi * beta_rel * Harmonic_Num * np.sqrt(Z**2 * RF_Voltage**2 - Energy_loss**2))))

    def ion_BunchLength(self, Circumference, Harmonic_Num, Energy_total, SlipF, Sigma_E, beta_rel, RF_Voltage, Energy_loss, Z):
        """
        General function to calculate bunch length of ions for given parameters  
        """
        return Circumference / (2. * np.pi * Harmonic_Num) * np.arccos(1 - (Sigma_E**2 * Energy_total * abs(SlipF) * Harmonic_Num * np.pi) / (beta_rel**2 * Z * RF_Voltage))

    def ion_EnergySpread(self, Circumference, Harmonic_Num, Energy_total, SlipF, BL, beta_rel, RF_Voltage, Energy_loss, Z):
        """
        Calculate energy spread for ions
        """
        tau_phi = 2 * np.pi * Harmonic_Num * BL / Circumference #bunch length in rad
        return np.sqrt(beta_rel**2 * Z * RF_Voltage * (-(np.cos(tau_phi)-1))/(Energy_total * abs(SlipF) * Harmonic_Num * np.pi))


class KineticIBS():

    def __init__(self, *args, **kwargs):
        pass

    def _Phi(self, beta , alpha , eta , eta_d):
        return eta_d + alpha * eta / beta

    def _Hi(self, beta, alpha, eta, eta_d):
        return (1 / beta) * ( eta**2 + (beta * eta_d + alpha * eta)**2 )

    def set_beam_parameters(self, Ncharges, Npart, Total_Energy):
        E0p = physical_constants["proton mass energy equivalent in MeV"][0]*1e-3

        self.Npart  = Npart
        self.Ncharg = Ncharges
        self.EnTot  = Total_Energy
        self.E_rest = 193.6999
        self.gammar = self.EnTot / self.E_rest
        self.betar  = np.sqrt(1 - 1/self.gammar**2)
        self.mass_i = (self.E_rest * m_p) / E0p   #Ions Mass
        self.c_rad  = (self.Ncharg * scipy.constants.e)**2 / (4 * np.pi * scipy.constants.epsilon_0 * c**2 * self.mass_i)

    def set_optic_functions(self, position, dels, bet_x, bet_y, alf_x, alf_y, eta_x, eta_dx, eta_y, eta_dy):
        self.posit  = position
        self.Circu  = position[len(position)-1]
        self.bet_x  = bet_x
        self.bet_y  = bet_y
        self.eta_x  = eta_x
        self.eta_dx = eta_dx
        self.eta_y  = eta_y
        self.alf_x  = alf_x
        self.alf_y  = alf_y
        self.dels   = dels
        self.DimT   = len(position)
        self.H_x    = self._Hi(bet_x, alf_x, eta_x, eta_dx)
        self.H_y    = self._Hi(bet_y, alf_y, eta_y, eta_dy)
        self.phi_x  = self._Phi(bet_x, alf_x, eta_x, eta_dx)
        self.phi_y  = self._Phi(bet_y, alf_y, eta_y, eta_dy)
        self.bx_bar = sum(bet_x * dels) / self.Circu
        self.by_bar = sum(bet_y * dels) / self.Circu
        self.dx_bar = sum(eta_x * dels) / self.Circu
        self.dy_bar = sum(eta_y * dels) / self.Circu
        self.frev   = self.betar * c / position[len(position)-1]

    def meanCoulogConst(self, Emit_x, Emit_y, Sig_M, BunchL):
        Etrans = 5e8 * (self.gammar * self.EnTot - self.E_rest) * (Emit_x / self.bx_bar)
        TempeV = 2.0 * Etrans
        sigxcm = 100 * np.sqrt(Emit_x * self.bx_bar + (self.dx_bar * Sig_M)**2)
        sigycm = 100 * np.sqrt(Emit_y * self.by_bar + (self.dy_bar * Sig_M)**2)
        sigtcm = 100 * BunchL
        volume = 8.0 * np.sqrt(np.pi**3) * sigxcm * sigycm * sigtcm
        densty = self.Npart  / volume
        debyul = 743.4 * np.sqrt(TempeV / densty) / self.Ncharg
        rmincl = 1.44e-7 * self.Ncharg**2 / TempeV
        rminqm = hbar * c * 1e5 / (2.0 * np.sqrt(2e-3 * Etrans * self.E_rest))
        rmin   = max(rmincl, rminqm)
        rmax   = min(sigxcm, debyul)
        coulog = np.log(rmax / rmin)
        Ncon   = self.Npart * self.c_rad**2 * c / (8 * np.pi * self.betar**3 * self.gammar**4 * Emit_x * Emit_y * BunchL * Sig_M)
        return Ncon * coulog
    
    def CoulogConst(self, Emit_x, Emit_y, Sig_M, BunchL, ind):
        Etrans = 5e8 * (self.gammar * self.EnTot - self.E_rest) * (Emit_x / self.bet_x[ind])
        TempeV = 2.0 * Etrans
        sigxcm = 100 * np.sqrt(Emit_x * self.bet_x[ind] + (self.eta_x[ind] * Sig_M)**2)
        sigycm = 100 * np.sqrt(Emit_y * self.bet_y[ind] + (self.eta_y[ind] * Sig_M)**2)
        sigtcm = 100 * BunchL
        volume = 8.0 * np.sqrt(np.pi**3) * sigxcm * sigycm * sigtcm
        densty = self.Npart  / volume
        debyul = 743.4 * np.sqrt(TempeV / densty) / self.Ncharg
        rmincl = 1.44e-7 * self.Ncharg**2 / TempeV
        rminqm = hbar * c * 1e5 / (2.0 * np.sqrt(2e-3 * Etrans * self.E_rest))
        rmin   = max(rmincl, rminqm)
        rmax   = min(sigxcm, debyul)
        coulog = np.log(rmax / rmin)
        Ncon   = self.Npart * self.c_rad**2 * c / (8 * np.pi * self.betar**3 * self.gammar**4 * Emit_x * Emit_y * BunchL * Sig_M)
        return Ncon * coulog

    def L_matrix(self, ind, Emit_x, Emit_y, Sig_M, lam_lim=20.):
        T1 = self.bet_x[ind] / Emit_x
        T2 = self.bet_y[ind] / Emit_y
        L = np.array([[T1, 0., - T1 * self.phi_x[ind] * self.gammar],
                      [0., T2, - T2 * self.phi_y[ind] * self.gammar],
                      [- T1 * self.phi_x[ind] * self.gammar, - T2 * self.phi_y[ind] * self.gammar, self.gammar**2 * (self.H_x[ind] / Emit_x + self.H_y[ind] / Emit_y + 1 / Sig_M**2)]])
        self.LL = L
        self.uplim = np.amax(self.LL) * lam_lim

    def det_L(self, lam):
        det = (((self.LL[0][0] + lam) * (self.LL[1][1] + lam) * (self.LL[2][2] + lam)) - 
               (self.LL[0][2] * (self.LL[1][1] + lam) * self.LL[2][0]) - 
               ((self.LL[0][0] + lam) * self.LL[1][2] * self.LL[2][1]))
        return det

    def fr_x(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * ((self.LL[1][1] + lam) * (self.LL[2][2] + lam) - self.LL[1][2] * self.LL[1][2])

    def fr_y(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * ((self.LL[0][0] + lam) * (self.LL[2][2] + lam) - self.LL[0][2] * self.LL[0][2])

    def fr_z(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * ((self.LL[0][0] + lam) * (self.LL[1][1] + lam))

    def fr_xz(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * ((self.LL[1][1] + lam) * self.LL[0][2] * (- 1.))

    def friction(self):
        kx = integrate.quad(self.fr_x, 0, self.uplim)[0]
        ky = integrate.quad(self.fr_y, 0, self.uplim)[0]
        kz = integrate.quad(self.fr_z, 0, self.uplim)[0]
        kxz = integrate.quad(self.fr_xz, 0, self.uplim)[0]
        return kx, ky, kz, kxz
    
    def diff_xx(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * ((self.LL[0][0] + lam) * (self.LL[2][2] + lam) - self.LL[0][2] * self.LL[0][2] + (self.LL[0][0] + lam) * (self.LL[1][1] + lam))

    def diff_yy(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * ((self.LL[1][1] + lam) * (self.LL[2][2] + lam) - self.LL[1][2] * self.LL[1][2] + (self.LL[0][0] + lam) * (self.LL[1][1] + lam))

    def diff_zz(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * ((self.LL[1][1] + lam) * (self.LL[2][2] + lam) - self.LL[1][2] * self.LL[1][2] + (self.LL[0][0] + lam) * (self.LL[2][2] + lam))

    def diff_zx(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * (self.LL[1][1] + lam) * self.LL[0][2]

    def diff_zy(self, lam):
        det = self.det_L(lam)
        return np.sqrt(lam / det) / det * (self.LL[0][0] + lam) * self.LL[1][2]

    def diffusion(self):
        dxx = integrate.quad(self.diff_xx, 0, self.uplim)[0]
        dyy = integrate.quad(self.diff_yy, 0, self.uplim)[0]
        dzz = integrate.quad(self.diff_zz, 0, self.uplim)[0]
        dzx = integrate.quad(self.diff_zx, 0, self.uplim)[0]
        dzy = integrate.quad(self.diff_zy, 0, self.uplim)[0]
        return dxx, dyy, dzz, dzx, dzy

    def evaluate_coefficients(self, Emit_x, Emit_y, Sig_M, BunchL):
        GRx, GRy, GRz = [], [], []
        FRx, FRy, FRz = [], [], []
        for j in range(self.DimT):
            ACon = self.CoulogConst(Emit_x, Emit_y, Sig_M, BunchL, j)
            #ACon = self.meanCoulogConst(Emit_x, Emit_y, Sig_M, BunchL)
            
            self.L_matrix(j, Emit_x, Emit_y, Sig_M, 20.)
            
            # !---------- Friction ----------!
            Fx, Fy, Fz, Fxz = self.friction()
            Kxx = ACon * Fx * self.bet_x[j] / Emit_x
            Kyy = ACon * Fy * self.bet_y[j] / Emit_y
            Kzz = ACon * Fz / Sig_M**2
            Kxz = ACon * Fxz

            # !--------- Diffusion ---------!
            dxx, dyy, dzz, dzx, dzy = self.diffusion()
            Dxx = ACon * dxx
            Dyy = ACon * dyy
            Dzz = ACon * dzz
            Dxz = ACon * dzx
            Dyz = ACon * dzy

            GRx.append(self.bet_x[j] / Emit_x * Dxx + self.gammar**2 * self.H_x[j] / Emit_x * Dzz  - 2 * self.bet_x[j] * self.phi_x[j] * self.gammar / Emit_x * Dxz)
            GRy.append(self.bet_y[j] / Emit_y * Dyy + self.gammar**2 * self.H_y[j] / Emit_y * Dzz  - 2 * self.bet_y[j] * self.phi_y[j] * self.gammar / Emit_y * Dyz)
            GRz.append(self.gammar**2 / Sig_M**2 * Dzz)
            
            FRx.append(self.bet_x[j] / Emit_x * (2 * Emit_x / self.bet_x[j] * Kxx) + self.gammar**2 * self.H_x[j] / Emit_x * (2 * Sig_M**2 * Kzz) - 2 * self.bet_x[j] * self.phi_x[j] * self.gammar / Emit_x * 2 * Kxz)
            FRy.append(self.bet_y[j] / Emit_y * (2 * Emit_y / self.bet_y[j] * Kyy) + self.gammar**2 * self.H_y[j] / Emit_y * (2 * Sig_M**2 * Kzz))
            FRz.append(2 * self.gammar**2 * Kzz)
    
        # !--------- Diffusion ---------!
        self.Dx = np.sum(GRx * self.dels) / self.Circu
        self.Dy = np.sum(GRy * self.dels) / self.Circu
        self.Dz = np.sum(GRz * self.dels) / self.Circu

        # !---------- Friction ----------!
        self.Fx = np.sum(FRx * self.dels) / self.Circu
        self.Fy = np.sum(FRy * self.dels) / self.Circu
        self.Fz = np.sum(FRz * self.dels) / self.Circu

    def kinetic_kick(self, part_coord_dict, alpha_x, alpha_y, dt):
        #print(part_coord_dict.shape[0])
        #print(p_std_x)
        #print(self.Dx)
        
        Ran1 = np.random.normal(loc = 0, scale = 1, size = part_coord_dict.shape[0])
        Ran2 = np.random.normal(loc = 0, scale = 1, size = part_coord_dict.shape[0])
        Ran3 = np.random.normal(loc = 0, scale = 1, size = part_coord_dict.shape[0])

        p_std_x = np.std(part_coord_dict['px']) / np.sqrt(1 + alpha_x**2)
        p_std_y = np.std(part_coord_dict['py']) / np.sqrt(1 + alpha_y**2)
        p_std_z = np.std(part_coord_dict['pz'])

        # !---------- Friction ----------!
        part_coord_dict['px'] -= self.Fx * part_coord_dict['px'] * dt 
        part_coord_dict['py'] -= self.Fy * part_coord_dict['py'] * dt 
        part_coord_dict['pz'] -= self.Fz * part_coord_dict['pz'] * dt
                         
        # !---------- Diffusion ----------!
        part_coord_dict['px'] += p_std_x * np.sqrt(2 * dt * self.Dx) * Ran1
        part_coord_dict['py'] += p_std_y * np.sqrt(2 * dt * self.Dy) * Ran2
        part_coord_dict['pz'] += p_std_z * np.sqrt(2 * dt * self.Dz) * Ran3    
