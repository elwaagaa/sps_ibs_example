"""
Plot the intensity of the IBS module tests for given intensity and number of turns
- Protons
"""
import matplotlib.pylab as plt
import pandas as pd
import numpy as np

# Select destination folder depending on the parameter values chosen 
save_fig = True

# Set initial parameters 
intensity_factor = 0.5
# ----- Set initial parameters -----
n_turns = 100000
bunch_intensity = intensity_factor*1e11/3 #1e11/3 is the original
sigma_z = 22.5e-2/3 #22.5e-2/3 is the original value

# Define plot parameters 
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# Load the saved dataframes
dfs_kinetic = []
dfs_analytical = []

# Load stored analytical values 
df_analytical = pd.read_parquet("xsuite_analytical_{}.parquet".format(n_turns))
tbt_checks = pd.read_parquet("tbt_checks_{}_turns.parquet".format(n_turns))

# Loop over the stored kinetic kicks 
for j in range(3):
    df = pd.read_parquet("xsuite_run{}_kinetic_{}.parquet".format(j, n_turns))
    dfs_kinetic.append(df)

# ----------------------- PLOT THE FIGURE ---------------------------------------------
fl_kinetic=True
fl_analytical=True

plot_average = True

if fl_analytical:
    analytical = df_analytical

f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (16,5))
f.suptitle('SPS Proton tracking: I = {:.2e}, sigma_z = {:.3f}'.format(bunch_intensity, sigma_z), fontsize=20)

# Find the average kinetic kick
All_epsx = np.array([dfs_kinetic[0]['eps_x'], dfs_kinetic[1]['eps_x'], dfs_kinetic[2]['eps_x']])
All_epsy = np.array([dfs_kinetic[0]['eps_y'], dfs_kinetic[1]['eps_y'], dfs_kinetic[2]['eps_y']])
All_sig_delta = np.array([dfs_kinetic[0]['sig_delta'], dfs_kinetic[1]['sig_delta'], dfs_kinetic[2]['sig_delta']])
eps_x_mean = np.mean(All_epsx, axis=0)
eps_y_mean = np.mean(All_epsy, axis=0)
sig_delta_mean = np.mean(All_sig_delta, axis=0)

# ax1.plot(nag[0], 'r')
plt.sca(ax1)
if fl_kinetic:
    if plot_average:
        plt.plot(eps_x_mean, alpha=0.7, label='Mean Kinetic', c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['eps_x'].values, alpha=0.7, label=f'Kinetic {k+1}')
if fl_analytical:
    plt.plot(analytical['eps_x'].values, c='k', label='Analytical')
plt.legend(fontsize=12)

plt.sca(ax2)
if fl_kinetic:
    if plot_average:
        plt.plot(eps_y_mean, alpha=0.7, c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['eps_y'].values, alpha=0.7)
if fl_analytical:
    plt.plot(analytical['eps_y'].values, c='k')

plt.sca(ax3)
if fl_kinetic:
    if plot_average:
        plt.plot(sig_delta_mean*1e3, alpha=0.7, c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['sig_delta'].values*1e3, alpha=0.7)
if fl_analytical:
    plt.plot(analytical['sig_delta'].values*1e3, c='k')

ax1.set_ylabel(r'$\varepsilon_x$ [m]')
ax1.set_xlabel('Turns')

ax2.set_ylabel(r'$\varepsilon_y$ [m]')
ax2.set_xlabel('Turns')

ax3.set_ylabel(r'$\sigma_{\delta}$ [$10^{-3}$]')
ax3.set_xlabel('Turns')

plt.tight_layout()
plt.show()
if save_fig:
    f.savefig("Emittances_SPS_Proton_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)
    
# Plot the integral evolution
fig = plt.figure(figsize=(10, 7))
fig.suptitle('Nagaitsev Integrals')
ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.plot(tbt_checks['Ixx'], c='b', label='Ixx')
ax.plot(tbt_checks['Iyy'], c='g', label='Iyy')
ax.plot(tbt_checks['Ipp'], c='k', label='Ipp')
ax.set_xlabel('Turns')
ax.legend()
if save_fig:
    fig.savefig("Nagaitsev_integrals_SPS_Proton_IBS_tracking_{}_turns.png".format(n_turns), dpi=250)


    
