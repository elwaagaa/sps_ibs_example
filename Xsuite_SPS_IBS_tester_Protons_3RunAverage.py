#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
SPS Q20 protons: IBS class tester with Xsuite, with the kinetic kick averaged over three runs 

This version uses line and particle input from Xsuite model of SPS sequence, including correct normalized emittance
"""
import json
import numpy as np
import xobjects as xo
import xtrack as xt
import xpart as xp
import pandas as pd
import matplotlib.pylab as plt

from IBS import NagaitsevIBS

# Flags to save figures
save_fig = True # individual plotter script for each case exists in respective destination folder

## Load xsuite line
with open("SPS_sequence/SPS_2021_LHC_q20_for_tracking.json") as fid:
    dd=json.load(fid)
line = xt.Line.from_dict(dd)
p0 = line.particle_ref

# Select destination folder depending on the parameter values chosen 
destination_folder = 'Convergence_tests_protons/1000times_intensity_3RunAverage' 
intensity_factor = 1000.0

# ----- Set initial parameters -----
n_turns = 20000 #00
bunch_intensity = intensity_factor*1e11/3 #1e11/3 is the original
sigma_z = 22.5e-2/3 #22.5e-2/3 is the original value
n_part = int(2000)
# For emittance: for SPS ions, use normalized emittance from Hannes and Isabelle's table 
nemitt_x= 2.5e-6
nemitt_y= 2.5e-6
Harmonic_Num = 4620
Energy_loss = 0
RF_Voltage = 3.0 # updated according to discussions with Alexandre Lasheen
IBS_step = 50.0 # turns to recompute IBS kick
# ----------------------------------

## Choose a context
context = xo.ContextCpu()         # For CPU

## Transfer lattice on context and compile tracking code
tracker = xt.Tracker(_context=context, line=line)
tracker.optimize_for_tracking()
particles0 = xp.generate_matched_gaussian_bunch(
         num_particles=n_part, total_intensity_particles=bunch_intensity,
         nemitt_x=nemitt_x, nemitt_y=nemitt_y, sigma_z=sigma_z,
         particle_ref=p0, tracker=tracker)


tw = tracker.twiss(particle_ref = p0)
dfs_kinetic = []
dfs_analytical = []

# Initialize benchmark dictinary 
check_keys = {'epsn_x', 'epsn_y', 'particles', 'Ixx', 'Iyy', 'Ipp', 'kinTx', 'kinTy', 'kinTz'}
tbt_checks = {nn: np.zeros((n_turns), dtype = float) for nn in check_keys}


#%% First investigate KINETIC mode 
mode = 'kinetic'
for j in range(3):
    print(f"\nRUN: {j+1}\n")
    
    particles = particles0.copy()
    
    # ----- Initialize IBS -----
    IBS = NagaitsevIBS()
    IBS.set_beam_parameters(particles)
    IBS.set_optic_functions(tw)
    
    dt = 1./IBS.frev # consecutive turns/frev, used only for analytical, 
    
    ## Initialize dictionaries
    record_emit = {'eps_x', 'eps_y', 'sig_delta', 'bl'}
    turn_by_turn = {nn: np.zeros((n_turns), dtype = float) for nn in record_emit}
    
    # --- Initialize 
    sig_x = np.std(particles.x[particles.state > 0])
    sig_y = np.std(particles.y[particles.state > 0])
    sig_delta = np.std(particles.delta[particles.state > 0])
    turn_by_turn['bl'][0]        = np.std(particles.zeta[particles.state > 0])
    turn_by_turn['sig_delta'][0] = sig_delta
    
    # Decide whether to use manual calculation of emittance or the StatisticalEmittance 
    turn_by_turn['eps_x'][0] = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
    turn_by_turn['eps_y'][0] = sig_y**2 / tw['bety'][0] 
    
    tbt_checks['epsn_x'][0] = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
    tbt_checks['epsn_y'][0] = sig_y**2 / tw['bety'][0] 
    tbt_checks['particles'][0] = sum(particles.state > 0)

    
    for i in range(1, n_turns):
    
        print(f'Run {j+1}: Turn = {i}')
        print('N_part = ',len(particles.x[particles.state > 0]))
    
        # Calculate normalized emittances 
        sig_x = np.std(particles.x[particles.state > 0])
        sig_y = np.std(particles.y[particles.state > 0])
        sig_delta = np.std(particles.delta[particles.state > 0])
        turn_by_turn['bl'][i]        = np.std(particles.zeta[particles.state > 0])
        turn_by_turn['sig_delta'][i] = sig_delta
        
        # Decide whether to use manual calculation of emittance or the StatisticalEmittance 
        turn_by_turn['eps_x'][i]     = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
        turn_by_turn['eps_y'][i]     = sig_y**2 / tw['bety'][0] 
        
        tbt_checks['epsn_x'][i] = turn_by_turn['eps_x'][i]  
        tbt_checks['epsn_y'][i] = turn_by_turn['eps_y'][i]
        tbt_checks['particles'][i] = sum(particles.state > 0)
        
        # Apply the kinetic kick from the calculated coefficient 
        if (i % IBS_step == 0) or (i==1):
           IBS.calculate_kinetic_coefficients(particles)
        IBS.apply_kinetic_kick(particles)

        # Check the growth rates 
        tbt_checks['kinTx'][i] = IBS.kinTx
        tbt_checks['kinTy'][i] = IBS.kinTy
        tbt_checks['kinTz'][i] = IBS.kinTz
    
        tracker.track(particles)

    Emitt = []
    Emitt.append(turn_by_turn['eps_x'])
    Emitt.append(turn_by_turn['eps_y'])
    Emitt.append(turn_by_turn['sig_delta'])
    Emitt.append(turn_by_turn['bl'])
    Emitt.append(turn_by_turn['kinTx'])
    Emitt.append(turn_by_turn['kinTy'])
    Emitt.append(turn_by_turn['kinTz'])
    
    df = pd.DataFrame(np.array(Emitt).T, columns=["eps_x", "eps_y", "sig_delta", "bl", "kinTx", "kinTy", "kinTz"])
    df.index.name = 'Turn'
    df.to_parquet("{}/xsuite_run{}_{}_{}.parquet".format(destination_folder, j, mode, n_turns))
    dfs_kinetic.append(df)
  
    
#%% Then investigate ANALYTICAL kick
mode = 'analytical'
print(f"Model: {mode}")

particles = particles0.copy()

# ----- Initialize IBS -----
IBS = NagaitsevIBS()
IBS.set_beam_parameters(particles)
IBS.set_optic_functions(tw)

dt = 1./IBS.frev # consecutive turns/frev, used only for analytical, 

## Initialize dictionaries
record_emit = {'eps_x', 'eps_y', 'sig_delta', 'bl'}
turn_by_turn = {nn: np.zeros((n_turns), dtype = float) for nn in record_emit}

# --- Initialize 
sig_x = np.std(particles.x[particles.state > 0])
sig_y = np.std(particles.y[particles.state > 0])
sig_delta = np.std(particles.delta[particles.state > 0])
turn_by_turn['bl'][0]        = np.std(particles.zeta[particles.state > 0])
turn_by_turn['sig_delta'][0] = sig_delta

# Decide whether to use manual calculation of emittance or the StatisticalEmittance 
turn_by_turn['eps_x'][0] = (sig_x**2 - (tw['dx'][0] * sig_delta)**2) / tw['betx'][0]
turn_by_turn['eps_y'][0] = sig_y**2 / tw['bety'][0] 
tbt_checks['Ixx'][0] = 0.
tbt_checks['Iyy'][0] = 0.
tbt_checks['Ixx'][0] = 0. 

for i in range(1, n_turns):

    print('Turn = ', i)
    print('N_part = ',len(particles.x[particles.state > 0]))

    # Calculate the IBS integrals and add the kick 
    if (i % IBS_step == 0) or (i==1):
         IBS.calculate_integrals(turn_by_turn['eps_x'][i-1],turn_by_turn['eps_y'][i-1],turn_by_turn['sig_delta'][i-1],turn_by_turn['bl'][i-1])
    Emit_x, Emit_y, Sig_M = IBS.emit_evol(turn_by_turn['eps_x'][i-1],turn_by_turn['eps_y'][i-1],turn_by_turn['sig_delta'][i-1],turn_by_turn['bl'][i-1], dt)
    
    Sigma_E = Sig_M*IBS.betar**2
    BunchL = IBS.BunchLength(IBS.Circu, Harmonic_Num, IBS.EnTot, IBS.slip, 
                   Sigma_E, IBS.betar, RF_Voltage*1e-3, Energy_loss, IBS.Ncharg)
    
    turn_by_turn['bl'][i]        = BunchL
    turn_by_turn['sig_delta'][i] = Sig_M
    turn_by_turn['eps_x'][i]     = Emit_x
    turn_by_turn['eps_y'][i]     = Emit_y
  
    tbt_checks['Ixx'][i] = IBS.Ixx
    tbt_checks['Iyy'][i] = IBS.Iyy
    tbt_checks['Ixx'][i] = IBS.Ipp  

    tracker.track(particles)

Emitt = []
Emitt.append(turn_by_turn['eps_x'])
Emitt.append(turn_by_turn['eps_y'])
Emitt.append(turn_by_turn['sig_delta'])
Emitt.append(turn_by_turn['bl'])

df_analytical = pd.DataFrame(np.array(Emitt).T, columns=["eps_x", "eps_y", "sig_delta", "bl"])
df_analytical.index.name = 'Turn'
df_analytical.to_parquet("{}/xsuite_{}_{}.parquet".format(destination_folder, mode, n_turns))
        
# Also save the TBT check
df_tbt = pd.DataFrame(tbt_checks)
df_tbt.to_parquet("{}/tbt_checks_{}_turns.parquet".format(destination_folder, n_turns))
  
# ------------------------ DEFINE PLOT PARAMETERS --------------------------------------
SMALL_SIZE = 20
MEDIUM_SIZE = 20
BIGGER_SIZE = 20
plt.rcParams["font.family"] = "serif"
plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
plt.rc('axes', titlesize=BIGGER_SIZE)    # fontsize of the axes title
plt.rc('axes', labelsize=BIGGER_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)   # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)   # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


# ----------------------- PLOT THE FIGURE ---------------------------------------------
fl_kinetic=True
fl_analytical=True

plot_average = True

if fl_analytical:
    analytical = df_analytical

f, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize = (16,5))
f.suptitle('SPS proton tracking: I = {:.2e}, sigma_z = {:.3f}'.format(bunch_intensity, sigma_z), fontsize=20)

# Find the average kinetic kick
All_epsx = np.array([dfs_kinetic[0]['eps_x'], dfs_kinetic[1]['eps_x'], dfs_kinetic[2]['eps_x']])
All_epsy = np.array([dfs_kinetic[0]['eps_y'], dfs_kinetic[1]['eps_y'], dfs_kinetic[2]['eps_y']])
All_sig_delta = np.array([dfs_kinetic[0]['sig_delta'], dfs_kinetic[1]['sig_delta'], dfs_kinetic[2]['sig_delta']])
eps_x_mean = np.mean(All_epsx, axis=0)
eps_y_mean = np.mean(All_epsy, axis=0)
sig_delta_mean = np.mean(All_sig_delta, axis=0)

# ax1.plot(nag[0], 'r')
plt.sca(ax1)
if fl_kinetic:
    if plot_average:
        plt.plot(eps_x_mean, alpha=0.7, label='Mean Kinetic', c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['eps_x'].values, alpha=0.7, label=f'Kinetic {k+1}')
if fl_analytical:
    plt.plot(analytical['eps_x'].values, c='k', label='Analytical')
plt.legend(fontsize=12)

plt.sca(ax2)
if fl_kinetic:
    if plot_average:
        plt.plot(eps_y_mean, alpha=0.7, c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['eps_y'].values, alpha=0.7)
if fl_analytical:
    plt.plot(analytical['eps_y'].values, c='k')

plt.sca(ax3)
if fl_kinetic:
    if plot_average:
        plt.plot(sig_delta_mean*1e3, alpha=0.7, c='b')
    else:
        for k, kinetic in enumerate(dfs_kinetic):
            plt.plot(kinetic['sig_delta'].values*1e3, alpha=0.7)
if fl_analytical:
    plt.plot(analytical['sig_delta'].values*1e3, c='k')

ax1.set_ylabel(r'$\varepsilon_x$ [m]')
ax1.set_xlabel('Turns')

ax2.set_ylabel(r'$\varepsilon_y$ [m]')
ax2.set_xlabel('Turns')

ax3.set_ylabel(r'$\sigma_{\delta}$ [$10^{-3}$]')
ax3.set_xlabel('Turns')

plt.tight_layout()
plt.show()
if save_fig:
    f.savefig("{}/Emittances_SPS_proton_IBS_tracking_{}_turns.png".format(destination_folder, n_turns), dpi=250)
    
# Plot the integral evolution
fig = plt.figure(figsize=(10, 7))
fig.suptitle('Nagaitsev Integrals')
ax = fig.add_subplot(1, 1, 1)  # create an axes object in the figure
ax.plot(tbt_checks['Ixx'], c='b', label='Ixx')
ax.plot(tbt_checks['Iyy'], c='g', label='Iyy')
ax.plot(tbt_checks['Ipp'], c='k', label='Ipp')
ax.set_xlabel('Turns')
ax.legend()
if save_fig:
    fig.savefig("{}/Nagaitsev_integrals_SPS_proton_IBS_tracking_{}_turns.png".format(destination_folder, n_turns), dpi=250)


    
