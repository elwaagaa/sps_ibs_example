#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Functions suggested by the OpenAI chatbot to improve the elliptic integral calculations
"""

import numpy as np
from scipy.integrate import fixed_quad

def RDiter(self, x, y, z):
    """
    Calculate elliptic integrals for the Nagaitsev integrals from ChatBot tip
    """
    R = []
    for i, j, k in zip(x, y, z):
        x0 = i
        y0 = j
        z0 = k
        if (x0 < 0) and (y0 <= 0) and (z0 <= 0):
            print('Elliptic Integral Calculation Failed. Wrong input values!')
            return

        # Compute auxiliary variables
        xy = x0 * y0
        xz = x0 * z0
        yz = y0 * z0
        lxyz = xy + xz + yz
        xyz = xy * z0
        r = z0 / (lxyz - 2 * xyz)
        s = (lxyz - xyz) / lxyz
        a = r * r - s
        b = r * (1 - r * r)
        c = s - r * r
        d = r * s
        e = (1 - r) * (1 - r) * (1 + r) * (1 + r)

        #print(r, s, a, b, c, d, e)
        # Compute Nagaitsev integral
        result = (1 / (2 * r)) * (np.log((1 + r) / (1 - r)) + b * np.arctan(r) + c * np.arctan(s) + d * np.arctan((r * s) / a))
        R.append(result)
    return R



# Fixed quad integration seems to give unreasonable values... 
def RD_integrate(self, x, y, z):
    """
    #Calculate elliptic integrals for the Nagaitsev integrals  - using 
    """
    R = []
    for i, j, k in zip(x, y, z):
        x0 = i
        y0 = j
        z0 = k
        if (x0 < 0) and (y0 <= 0) and (z0 <= 0):
            print('Elliptic Integral Calculation Failed. Wrong input values!')
            return
        result, _ = fixed_quad(self.nagaitsev_integrand, -1, 1, args=(x0, y0, z0))
        R.append(result)
    return R
    
    
def nagaitsev_integrand(self, t, x, y, z):
    """
    Give the Nagaitsev integral of elliptic integrals of the third kind
    """
    return (1 / (np.sqrt(x * y) + np.sqrt(x * z) + np.sqrt(y * z))) / (np.sqrt(1 - t**2))